#Interveiw  Test (About regions)
Create an Angular project thatdisplays a list of regions, countries and a simple table using Angular, TypeScript, Observables and NGRX.


## Installation

Run `npm install` for all installation for node_module
```
npm install
```

## Style In project
I had install Angular modules to use in this applicaiton for the dropdowns and buttons styling
```
ng add @angular/material
```
[Angular Material](https://material.angular.io/components/categories)
would have use the BEM for styling for naming css if project was a bigger scope so i just use most inline style for most part plus the global styling so the background doesnt blind me. Then i center the content  in the middle of the page.

## Description
This applicaiton should allow the user to navigate to a country based on a region and display some simple information.
1. Create a drop down that cotainers the "Region" options Europe and Asia hardcoded.
2. One seleecting the region a second drop down should be enabled and populated with "Countries" based on the API response from above URLS.
3. On selecting the "Country" display a simple table tht cotains the
a. Name
b. CapitalW
c. population
d. Currencies
e. Flag


## NGRX store
I include the ngrx/store, ngrx/efffect and ngrx/devtool. Provide the required modules for store, effect and devtools in ngModule. I use these ngrx features in my application to store all countries information. The way i go about doing this with ngrx is by dispatching an action from the component where have an action listener called an effect receives the required region_name and then provides that for the service. The servers then collect the information and dispatches another action in which goes to the reduces and updates the state. When the state has been updated i use the selector which selects allCountries property from the store and then provides it to the component. The component now has the list so it shows the list in the country drop down and the load button should also be display as well. After this had been completed then we need to select an item in the countries drop down and click the load button to display the country table.


## Issues
I was not sure we are suppose to display all of the currencies infromation so what i did was display the infromation about one of its feilds which is currencies code. I ran out of time. My test are at a 70%  and i could have easily tested functions to bring that percentage up. I run the command ng test --code-coverage to get a file which would show me the test specs that are not tested for the function, statement, branch and lines.
