import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Regions } from './regions.component';
import { StoreModule, Store } from '@ngrx/store';
import { reducers, State } from 'src/app/reducers';
import { MaterialModule } from 'src/app/material/material';
import { CountryDetail } from '../country-detail/country-detail.component';
import { CountriesService } from 'src/app/service/countries.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';
describe('Regions', () => {
  let component: Regions;
  let fixture: ComponentFixture<Regions>;
  let store: Store<State>;
  let select_feature: DebugElement[];
  beforeEach(async( () => {
    TestBed.configureTestingModule({
      declarations: [ Regions , CountryDetail],
      providers:[ CountriesService],
      imports:[
        StoreModule.forRoot(reducers),
        MaterialModule,
        BrowserAnimationsModule
      ]
    })
    .compileComponents();
    fixture = TestBed.createComponent(Regions);
    select_feature = fixture.debugElement.queryAll(By.css('mat-form-field'));
      // console.log(select_feature);
    store = TestBed.get(Store);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Regions);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should not show country dropdown',async(()=>{
    select_feature[0].triggerEventHandler('click',null);
    // fixture.detectChanges();
  }));

  describe('After region was click ', ()=>{

    beforeEach(async( ()=>{
      //simulate a click on contries after making a mock of the service provided to return dummy data instead
    }))
    it('should show contries from the service method in the store',()=>{
    // look on country list to see the if the value is there
    })
  });
});
