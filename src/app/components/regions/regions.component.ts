import { Component, OnInit } from '@angular/core';
import {Store, select} from  '@ngrx/store';
import {State} from  'src/app/reducers';
import { GetCountries } from 'src/app/state/countries.actions';

import { countries_selectors } from 'src/app/state/countries.selectors';

@Component({
  selector: 'regions',
  templateUrl: './regions.component.html',
  styleUrls: ['./regions.component.css']
})
export class Regions implements OnInit {
  public countries:any;
  public country:any;
  public displayButton: boolean = false;
  constructor(private store: Store<State>) { }


  ngOnInit() {
    this.store.pipe(select(countries_selectors)).subscribe((result)=>{
      this.countries = result;
    })
  }

  getCountries(region){
    this.store.dispatch(new GetCountries(region))
    this.displayButton = true;
  }

  selectCountry(country:string){
    //console.log(this.countries)
    this.country = this.countries.filter((result)=> result.name === country).pop();

  }

  showCountries(){
    return this.displayButton;
  }


}
