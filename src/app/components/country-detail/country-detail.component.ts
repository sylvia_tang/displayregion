import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'country-detail',
  templateUrl: './country-detail.component.html',
  styleUrls: ['./country-detail.component.css']
})
export class CountryDetail implements OnInit {
  //selected infromation is provided here
  @Input() country;
  public displayTable: boolean = false;
  //use selected infromation to provide withing the table in template
  constructor() { }

  ngOnInit() {}

  showTable(){
    this.displayTable = true;
  }

}
