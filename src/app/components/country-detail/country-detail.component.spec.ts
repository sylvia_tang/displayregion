import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CountryDetail} from './country-detail.component';


describe('CountryDetailComponent', () => {
  let component: CountryDetail;
  let fixture: ComponentFixture<CountryDetail>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CountryDetail ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CountryDetail);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
