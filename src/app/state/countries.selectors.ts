import {
  createSelector,
} from '@ngrx/store';
import { State } from '../reducers';

const selectContries = (state: State) => state;

export const countries_selectors = createSelector(
  selectContries,
  (state: any) => state.allCountries
);
