import {Action} from "@ngrx/store";
import * as types from './types';




export class GetCountries implements Action{
  readonly type = types.GET_COUNTRIES;
  constructor( readonly region_name: string){  }
}

export class GetCountriesError implements Action{
  readonly type = types.GET_COUNTRIES_ERROR;
  constructor( readonly error: any){}
}
export class GetCountriesSuccess implements Action{
  readonly type = types.GET_COUNTRIES_SUCCESS;
  constructor(readonly payload: any){ }
}
export type Actions = GetCountries | GetCountriesError | GetCountriesSuccess;
