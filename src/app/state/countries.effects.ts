import {Injectable} from '@angular/core';
import {Effect, ofType, Actions} from '@ngrx/effects';
import { of } from 'rxjs';
import { CountriesService } from "../service/countries.service";
import { switchMap , catchError} from 'rxjs/operators';
import * as countriesActions from './countries.actions';
import {GET_COUNTRIES} from './types';

@Injectable()
export class CountriesEffects{
  constructor(private _actions$: Actions, private service: CountriesService  ){}



  @Effect()
  AllContries = this._actions$.pipe(
    ofType(GET_COUNTRIES),
    switchMap( ( {region_name}  ) =>{
      return this.service.getCountries(region_name);;
    }),
    switchMap( result =>{
      return of(new countriesActions.GetCountriesSuccess(result)) ;
    } ),
    catchError((err)=>{
    return of(new countriesActions.GetCountriesError({status:err.status, message:err.message}));
    })
  );


}
