import {GetCountries, GetCountriesError, GetCountriesSuccess} from "./countries.actions";
import * as types from './types';


describe('GET COUNTRIES ACTIONS', () => {
  it('should dispatch the action of countries', () => {
    const action = new GetCountries("Hello");
    expect({ ...action} ).toEqual({ region_name: "Hello", type: types.GET_COUNTRIES });
  });
});
describe('GET COUNTRIES ERROR ACTION', () => {
  it('should get an error for contries', () => {
    const action = new GetCountriesError("error countries");
    expect({ ...action} ).toEqual({ error: "error countries", type: types.GET_COUNTRIES_ERROR  });
  });
});
describe('GET COUNTRIES SUCCESS ACTION', () => {
  it('should dispatch the action of countries', () => {
    const action = new GetCountriesSuccess("successful countries");
    expect({ ...action} ).toEqual({ payload: "successful countries", type: types.GET_COUNTRIES_SUCCESS  });
  });
});
