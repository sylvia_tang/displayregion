import { TestBed, async } from '@angular/core/testing';
import {  HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CountriesService } from '../service/countries.service';
import { provideMockActions } from '@ngrx/effects/testing';
import {provideMockStore} from '@ngrx/store/testing';
import { Subject, of, throwError } from 'rxjs';
import {GET_COUNTRIES} from './types';
import {CountriesEffects}  from './countries.effects';
import { Country } from '../interface/country';
import { GetCountriesSuccess, GetCountriesError } from './countries.actions';

describe('CountryEffect ', ()=>{
  let actions: Subject<any>;
  let effects: CountriesEffects;
  let service: CountriesService;
  beforeEach(() => {
  TestBed.configureTestingModule({
    providers: [
      CountriesEffects,
      provideMockActions(() => actions),
      CountriesService,
      provideMockStore()
    ],
    imports:[      HttpClientTestingModule]
  });

  effects = TestBed.get(CountriesEffects);
  service = TestBed.get(CountriesService);
});

it('should be created', async() => {
   expect(effects).toBeTruthy();
 });

 it('should dispatch from get countries and get infromation from the service ', async(async() => {
    actions = new Subject();
    const countryNavigatedAction = {
      type: GET_COUNTRIES,
      region_name: "dummy"
    };
    spyOn(service, 'getCountries').and.returnValue(of(dummy_countries));
    actions.next(countryNavigatedAction);
    effects.AllContries.subscribe((result: GetCountriesSuccess) => {
    expect(result.payload).toEqual(dummy_countries);
  });
}));



it('should dispatch and get an error ', async(async() => {
 actions = new Subject();
 const countryNavigatedAction = {
   type: GET_COUNTRIES,
   region_name: "dummy"
 };
 spyOn(service, 'getCountries').and.returnValue( throwError(dummy_error) );
 actions.next(countryNavigatedAction);
 effects.AllContries.subscribe((result: GetCountriesError) => {
 expect(result.error).toEqual(dummy_error);
});

}));
const dummy_error = {status: 400, message: "serivce error" };
const dummy_countries:Country[] =[
  {
  name:"Colorado",
  capital:"Denver",
  population:1000000,
  currencies: [{code: 'US'}],
  flag: 'url '
},
{
name:"New York",
capital:"New York",
population:10000000,
currencies: [{code: 'US'}],
flag: 'url '
}
]
})
