import  * as types from './types';
import {Actions} from './countries.actions';
import { Country } from '../interface/country';


export const init: any = [];

export function countriesReducer(state = init, action: Actions){
    switch(action.type){
      case types.GET_COUNTRIES_ERROR:
        return action.error;
      case types.GET_COUNTRIES_SUCCESS:
        return action.payload;
      default: return state;
    }
}
