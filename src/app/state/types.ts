
export const GET_COUNTRIES ="GET COUNTRIES [trigger the effect]";
export const GET_COUNTRIES_ERROR ="GET COUNTRIES_ERROR [when a error is thrown from server]";
export const GET_COUNTRIES_SUCCESS ="GET COUNTRIES_SUCCESS [when it is successful]";
