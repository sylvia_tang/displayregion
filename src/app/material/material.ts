import {NgModule} from '@angular/core';
import { MatSelectModule, MatButtonModule } from '@angular/material';

@NgModule({
  exports: [
    MatSelectModule,
    MatButtonModule
  ]
})
export class MaterialModule {}
