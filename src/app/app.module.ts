import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { Regions } from './components/regions/regions.component';
import { CountryDetail } from './components/country-detail/country-detail.component';
import { StoreModule } from '@ngrx/store';
import { reducers, metaReducers } from './reducers';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
// import { MatSelectModule, MatButtonModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { EffectsModule } from '@ngrx/effects';

import { HttpClientModule } from '@angular/common/http';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import { CountriesEffects } from './state/countries.effects';
import { MaterialModule } from './material/material';

@NgModule({
  declarations: [
    AppComponent,
    Regions,
    CountryDetail
  ],
  imports: [
    BrowserModule,
    StoreModule.forRoot(reducers),
    NoopAnimationsModule,
    MaterialModule,
    FormsModule,
    EffectsModule.forRoot([CountriesEffects]),
    HttpClientModule,
    BrowserAnimationsModule,
    StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: environment.production })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
