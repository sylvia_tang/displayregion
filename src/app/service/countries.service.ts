import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
/**
Using this service
 to collect country infromation
 from external api using http protocal of get.
*/

@Injectable({
  providedIn: 'root'
})
export class CountriesService {
  public url: string= 'https://restcountries.eu/rest/v2/region/';
  constructor(private http: HttpClient) { }


  getCountries(region:string):Observable<any>{
    return this.http.get(`${this.url}${region}`);
  }
}
