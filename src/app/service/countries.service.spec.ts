import { TestBed } from '@angular/core/testing';

import { CountriesService } from './countries.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { Country } from '../interface/country';

describe('CountriesService', () => {
  let service: CountriesService;
  let httpMock: HttpTestingController;
  beforeEach(() =>{
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [CountriesService]
    })
    service = TestBed.get(CountriesService);
    httpMock = TestBed.get(HttpTestingController);
  });

  it('should retrieve country from API get request', () => {
    service.getCountries('asia').subscribe(countries=>{
      expect(countries.length).toBe(2);
      expect(countries).toEqual(dummy_countries);
    })
    // expect(service).toBeTruthy();
    const request = httpMock.expectOne(`${service.url}asia`);

    expect(request.request.method).toBe('GET');
    request.flush(dummy_countries);
  });

  afterEach(()=>{
    httpMock.verify();
  })

  const dummy_countries:Country[] =[
    {
    name:"Colorado",
    capital:"Denver",
    population:1000000,
    currencies: [{code: 'US'}],
    flag: 'url '
  },
  {
  name:"New York",
  capital:"New York",
  population:10000000,
  currencies: [{code: 'US'}],
  flag: 'url '
  }
  ]
});
