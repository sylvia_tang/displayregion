import {
  ActionReducerMap,
  MetaReducer
} from '@ngrx/store';
import {countriesReducer} from '../state/countries.reducers';

import { environment } from '../../environments/environment';

export interface State {
    allCountries: any

}

export const reducers: ActionReducerMap<State> = {
  // reducers go here
  allCountries: countriesReducer,
};


export const metaReducers: MetaReducer<State>[] = !environment.production ? [] : [];
