import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { CountryDetail } from './components/country-detail/country-detail.component';
import { Regions } from './components/regions/regions.component';
import { StoreModule } from '@ngrx/store';
import { reducers } from './reducers';
import { MaterialModule } from './material/material';


describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        Regions,
        CountryDetail
      ],
      imports:[
        StoreModule.forRoot(reducers),
        MaterialModule
      ]
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });
});
