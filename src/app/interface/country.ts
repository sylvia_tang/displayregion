export interface Country {
   name:string ,
   capital:string ,
   population: number,
   currencies: any,
   flag: string
};

export interface Error{
  status: number,
  message?: string 
}
